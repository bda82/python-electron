import sys
import datetime
from flask import Flask
from flaskwebgui import FlaskUI
from flask import render_template
from flask_socketio import SocketIO
from flask_socketio import send as send_message
from flask_socketio import emit as emit_message

from forex_python.converter import CurrencyRates
from forex_python.bitcoin import BtcConverter

from config.configuration import Configuration

config = Configuration()

app = Flask(__name__)

app.config['SECRET_KEY'] = 'jhbHB6^&*&b87B*&bkBHJBjhb^&87887*&'
socketio = SocketIO(app, cors_allowed_origins="*")


class CurrencyRatesFetcher:

    currency = CurrencyRates()
    bitcoin = BtcConverter()

    @property
    def usd_rub(self):
        return self.currency.get_rate('USD', 'RUB')

    @property
    def eur_rub(self):
        return self.currency.get_rate("EUR", "RUB")
    
    @property
    def gbp_rub(self):
        return self.currency.get_rate("GBP", "RUB")

    @property
    def jpy_rub(self):
        return self.currency.get_rate("JPY", "RUB")

    @property
    def btc_rub(self):
        return self.bitcoin.get_latest_price('RUB')


currency_rates = CurrencyRatesFetcher()


@socketio.on("connect", namespace="/data")
def handle_connect():
    print("Client connected...")


@socketio.on("disconnect", namespace="/data")
def handle_disconnect():
    print("Client disconnected...")


@socketio.on("message", namespace="/data")
def handle_message_on_receive(message):
    print(f"Handle message on receive: {message}")
    answer = fetch_data()
    emit_message("message", {"data": answer})


def fetch_data():
    return {
        "USD": currency_rates.usd_rub,
        "EUR": currency_rates.eur_rub,
        "GBP": currency_rates.gbp_rub,
        "JPY": currency_rates.jpy_rub,
        "BTC": currency_rates.btc_rub,
        "updated": datetime.datetime.now().strftime("%y-%M-%d %H:%M:%S")
    }


@app.route("/")
def index():
    return render_template("index.html")


def main():
    ui = FlaskUI(app=app,
                 width=config.width,
                 height=config.height,
                 socketio=socketio)
    ui.run()



if __name__ == "__main__":
    sys.exit(main())

